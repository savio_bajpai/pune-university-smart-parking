package com.example.savio.smartparking;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by savio on 23/5/16.
 */
public class ContactUsFragment extends Fragment {

    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.content_contact_us,container,false);

        final TextView email=(TextView) view.findViewById(R.id.textView_email);

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // explicit 3rd party invoke
                    String[] recepients = { "joelbajpai7@gmail.com" };
                    Intent email = new Intent( Intent.ACTION_SEND, Uri.parse("mailto:") );
                    email.setType("message/rfc822");

                    email.putExtra( Intent.EXTRA_EMAIL, recepients );
                    email.putExtra(Intent.EXTRA_SUBJECT, "Temp");
                    email.putExtra(Intent.EXTRA_TEXT, "hey");

                    try{
                        startActivity( Intent.createChooser( email, "Choose an email client" ));
                    }catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(getActivity(), "No email client installed.",Toast.LENGTH_LONG).show();
                    }

            }
        });


        return  view;
    }
}
