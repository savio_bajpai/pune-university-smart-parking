package com.example.savio.smartparking;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ProgressBar progressBar = ( ProgressBar ) findViewById( R.id.progressBar );
        final TextView register_link =(TextView)findViewById(R.id.textView_register);
        final Button login = ( Button ) findViewById( R.id.button_login );

        if( login != null ){
            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //Toast.makeText(Login.this, "Sending mail", Toast.LENGTH_SHORT).show();
//                    Intent intent_register = new Intent(Login.this, MailSender.class);
//                    Login.this.startActivity(intent_register);
//                    finish();


                    Toast.makeText(Login.this, "mail Sent", Toast.LENGTH_SHORT).show();
                }
            });
        }
        if (register_link != null) {

            register_link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(Login.this, "Register", Toast.LENGTH_SHORT).show();
                    Intent intent_register = new Intent(Login.this, Register.class);
                    Login.this.startActivity(intent_register);
                }
            });
        }

    }

    protected  void sendEmail(){// explicit 3rd party invoke
        String[] recepients = { "joelbajpai7@gmail.com" };
        Intent email = new Intent( Intent.ACTION_SEND, Uri.parse("mailto:") );
        email.setType("message/rfc822");

        email.putExtra( Intent.EXTRA_EMAIL, recepients );
        email.putExtra(Intent.EXTRA_SUBJECT, "Temp");
        email.putExtra(Intent.EXTRA_TEXT, "hey");

        try{
            startActivity( Intent.createChooser( email, "Choose an email client" ));
        }catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(Login.this, "No email client installed.",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
       /* Intent intent = new Intent( getApplicationContext(), NavigationActivity.class );
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getApplicationContext().startActivity(intent);
        finish();*/

        super.onBackPressed();
        finish();
    }
}

