package com.example.savio.smartparking;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        invalidateOptionsMenu();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        if (drawer != null) {
            drawer.setDrawerListener(toggle);
        }
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        android.app.FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new CheckAvailability2()).commit();

        invalidateOptionsMenu();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation, menu);
        SharedPreferences sharedPreferences = getSharedPreferences( "Parking_global_data", Context.MODE_PRIVATE );
        String check = sharedPreferences.getString("Login", "" );

        if( check.equals( "Logged_out" ) || check.equals("")){
            menu.findItem( R.id.action_login).setVisible( true );
            menu.findItem( R.id.action_logout).setVisible( false );
            Log.i( "*A"," "+check );
            Toast.makeText( getApplicationContext(), "Logged_in value" + check, Toast.LENGTH_SHORT).show();
        }
        else{
            menu.findItem( R.id.action_login).setVisible( false );
            menu.findItem( R.id.action_logout).setVisible( true );
            Toast.makeText( getApplicationContext(), "Login value" + check, Toast.LENGTH_SHORT).show();
            Log.i( "*A"," "+check );
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        SharedPreferences.Editor editor = getSharedPreferences("Parking_global_data", Context.MODE_PRIVATE).edit();

        if (id == R.id.action_login) {
            Intent intent = new Intent( this, Login.class );
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            editor.putString("Login", "Logged_in");
            editor.putString("Name", "Savio");
            editor.putString("LastName", "Bajpai");
            editor.putString("Email", "savia@gmail.com");
            editor.putString("ContactNo", "9881481390");
            editor.apply();
            this.startActivity(intent);

            return true;
        }
        else if( id == R.id.action_logout) {
            invalidateOptionsMenu();
            editor.putString("Login", "Logged_out");
            editor.apply();
            recreate();
            Toast.makeText( getApplicationContext(), "Successfully Logged Out", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.nav_view_profile ) {
            Intent intent = new Intent( this, ViewProfile.class );
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(intent);
            finish();

        } else if (id == R.id.nav_about_us ) {

            Intent intent = new Intent( this, AboutUs.class );
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(intent);
            finish();

        } else if (id == R.id.nav_contact_us ) {

            Intent intent = new Intent( this, ContactUs.class );
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(intent);
            finish();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        invalidateOptionsMenu();
        super.onResume();
    }
}
