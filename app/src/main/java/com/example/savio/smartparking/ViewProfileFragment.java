package com.example.savio.smartparking;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by savio on 23/5/16.
 */
public class ViewProfileFragment extends Fragment {

    View view;
    AlertDialog.Builder alert;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_view_profile, container, false);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("Parking_global_data", Context.MODE_PRIVATE);
        final String check = sharedPreferences.getString("Login", "");

        final SharedPreferences.Editor editor = getActivity().getSharedPreferences("Parking_global_data", Context.MODE_PRIVATE).edit();

        final EditText first_name = (EditText) view.findViewById(R.id.editText_view_Name);
        final EditText last_name = (EditText) view.findViewById(R.id.editText_view_lastName);
        final EditText email = (EditText) view.findViewById(R.id.editText_view_email);
        final EditText phone = (EditText) view.findViewById(R.id.editText_view_phoneNo);

        first_name.setKeyListener(null);
        last_name.setKeyListener(null);
        email.setKeyListener(null);
        phone.setKeyListener(null);

        if (check.equals("Logged_out") || check.equals("")) {
            alert = new AlertDialog.Builder(getActivity());
            alert.setMessage("Please login to view profile");
            alert.setPositiveButton("Login", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(getActivity(), Login.class);
                    editor.putString("Login", "Logged_in");
                    editor.apply();
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getActivity().startActivity(intent);
                    dialog.dismiss();

                }
            });
            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(getActivity(), NavigationActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getActivity().startActivity(intent);
                    getActivity().finish();
                    dialog.dismiss();


                }
            });
            alert.show();

        }
        if(check.equals("Logged_in") )
        {

            first_name.setText(sharedPreferences.getString("Name", ""));
            last_name.setText(sharedPreferences.getString("LastName", ""));
            email.setText(sharedPreferences.getString("Email", ""));
            phone.setText(sharedPreferences.getString("ContactNo", ""));

        }

        final Button editprofile_button = (Button) view.findViewById(R.id.view_editprofile_button);
        editprofile_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), EditProfile.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });

        return  view;

    }

    @Override
    public void onResume() {


               super.onResume();

    }
}

